public class MergeSort {

    public static void main(String[] args) {
        int[] arrUnsorted = new int[]{12,14,1,2,3,5,4,7,9,5,4,12,14,15,16};
        int[] arrSorted = mergeSort(arrUnsorted);
    }

    public static int[] mergeSort(int[] arr) {
        int[] mergeSorted;
        int size1 = arr.length / 2;
        int size2 = arr.length - size1;
        int[] arrLeft = new int[size1];
        int[] arrRight = new int[size2];
        for (int i = 0; i < arr.length; i++) {
            if (i < size1) {
                arrLeft[i] = arr[i];
            } else {
                arrRight[i - size1] = arr[i];
            }
        }

        if (arrLeft.length > 2) {
            int[] arrLeftSorted = mergeSort(arrLeft);
            int[] arrRightSorted = mergeSort(arrRight);
            mergeSorted = merge(arrLeftSorted, arrRightSorted);
        }else{
            int[] arrLeftSorted = simpleSort(arrLeft);
            int[] arrRightSorted = simpleSort(arrRight);
            mergeSorted = merge(arrLeftSorted, arrRightSorted);
        }

        return mergeSorted;
    }


    public static int[] simpleSort(int[] arr) {
        if (arr.length == 2) {
            int[] sortedArr = new int[arr.length];

            if (arr[0] > arr[1]) {
                sortedArr[0] = arr[1];
                sortedArr[1] = arr[0];
                return sortedArr;
            } else {
                return arr;
            }
        }
        return arr;
    }

    public static int[] merge(int[] arr1, int[] arr2) {
        int[] mergedArr = new int[arr1.length + arr2.length];

        int i = 0;
        int j = 0;
        while (true) {
            if (i != arr1.length && j != arr2.length) {
                if (arr1[i] < arr2[j]) {
                    mergedArr[i + j] = arr1[i];
                    i++;
                } else {
                    mergedArr[i + j] = arr2[j];
                    j++;
                }
            }

            if (i == arr1.length && j != arr2.length) {
                mergedArr[i + j] = arr2[j];
                j++;
            }

            if (i != arr1.length && j == arr2.length) {
                mergedArr[i + j] = arr1[i];
                i++;
            }

            if (i == arr1.length && j == arr2.length) {
                return mergedArr;
            }
        }
    }
}
